## This repository is for the ESP32 that allows the user to control webasto remotely
It uses ESP32 with SIM800L modem, 1 channel relay, DHT22 and voltage divider. Modem is used for GPRS communication and voltage divider is used to detecting if the webasto is on or off. DHT22 sensor is used for temperature and humidity readings. Relay is used for sending 12V pulse to Telestart T91 module that will then control the webasto.

## Parts used in this project

1. 
    [LILYGO® TTGO T-Call V1.3 V1.4 ESP32 Wireless Module GPRS Antenna SIM Card SIM800L Board - CP2104 Chip
    ](https://www.banggood.com/LILYGO-TTGO-T-Call-V1_3-V1_4-ESP32-Wireless-Module-GPRS-Antenna-SIM-Card-SIM800L-Board-p-1527048.html?imageAb=1&rmmds=myorder&cur_warehouse=CZ&ID=6309941&DCC=FI&currency=EUR&akmClientCountry=FI)
2.  
    [1PC RJXHOBBY RF Connector Pigtail Cable SMA Female to uFL/u.FL/IPX/IPEX Antenna Extension Cable](https://www.banggood.com/1PC-RJXHOBBY-RF-Connector-Pigtail-Cable-SMA-Female-to-uFLu_FLIPXIPEX-Antenna-Extension-Cable-p-1335190.html?rmmds=myorder&cur_warehouse=CN)
3. [5cm 2.4Ghz Antenna WIFI 3dBi SMA Male 2.4G Antena SMA 824-960MHz 1710-1990MHz](https://www.banggood.com/5cm-2_4Ghz-Antenna-WIFI-3dBi-SMA-Male-2_4G-Antena-SMA-824-960MHz-1710-1990MHz-p-1531769.html?rmmds=myorder&cur_warehouse=CN)
4. [Jänniteanturi 0-25 V](https://www.elektroniikkaosat.com/c-67/p-927748375/Janniteanturi-0-25-V.html)
5. [Relelähtö, 1-kanavainen](https://www.elektroniikkaosat.com/c-45/p-986626317/Relelahto-1-kanavainen.html)
6. [DHT 22](https://nettigo.eu/products/dht22-humidity-and-temperature-sensor-breadboard-friendly)

## Things needed to edit in the secrets.h file

```
// Server details
const String api_key = "secret_api_key";

const String server = "sub.domain.tld";

// Your GPRS credentials
const char apn[] = "internet"; // Access Point Name for ISP default for DNA is "internet"
const char gprsUser[] = ""; // Username
const char gprsPass[] = ""; // Password
const char simPIN[] = "1234"; // SIM card PIN code, if any default for DNA is 1234

```

## These values should also be changed

This value determines the amount that the ESP32 sleeps between loops
```
#define TIME_TO_SLEEP 30 // in seconds
```
These are for the GPIO pins in the ESP32 so if you use other than these pins then you should change them to correspond to the real values.
```
const int relayPin = 32;
const int tempSensorPin = 13;
const int voltageSensorPin = 14;
```

## Image of the assembled product
![final_product](images/product.png)