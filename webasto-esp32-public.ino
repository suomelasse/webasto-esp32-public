#include <Wire.h>

#define SIM800L_IP5306_VERSION_20190610

#include "secrets.h"
#include "utilities.h"
#include "TinyGPS++.h"
#include <SoftwareSerial.h>

#include <DHT.h>
#define DHTTYPE DHT22
// Set serial for debug console (to the Serial Monitor, default speed 115200)
#define SerialMon Serial
// Set serial for AT commands (to the module)
#define SerialAT Serial1

// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM800          // Modem is SIM800
#define TINY_GSM_RX_BUFFER 1024   // Set RX buffer to 1KMb

#include <TinyGsmClient.h>

#include <ArduinoHttpClient.h>

#define uS_TO_S_FACTOR 1000000
#define TIME_TO_SLEEP 30 // in seconds 
TinyGsm modem(SerialAT);

const int port = 80;
TinyGsmClient client(modem);

const int relayPin = 32;
const int tempSensorPin = 13;
const int voltageSensorPin = 14;

DHT dht(tempSensorPin, DHTTYPE);

bool status, manualInterrupt, buttonInterrupt, hadPower;
RTC_DATA_ATTR bool timerOn;
int onTime, hasVoltage;
float voltageValue;

RTC_DATA_ATTR int pulseSent;

RTC_DATA_ATTR unsigned long startMillis;
unsigned long currentOnTime;
RTC_DATA_ATTR unsigned long deepSleepOffset;
unsigned long maxMillis;
unsigned long maxMillisOnOff;
unsigned long savedMaxMillis;

int h, m, day, month, year;
int endH, endM, endDay, endMonth, endYear;

int signal, savedPulseSent;

bool requestSuccess, onOffSuccess;

float temp, hum;

void turnOffNetlight()
{
    SerialMon.println("Turning off SIM800 Red LED...");
    modem.sendAT("+CNETLIGHT=0");
}

void setupModem()
{
#ifdef MODEM_RST
    // Keep reset high
    pinMode(MODEM_RST, OUTPUT);
    digitalWrite(MODEM_RST, HIGH);
#endif

    pinMode(MODEM_PWRKEY, OUTPUT);
    pinMode(MODEM_POWER_ON, OUTPUT);

    // Turn on the Modem power first
    digitalWrite(MODEM_POWER_ON, HIGH);

    // Pull down PWRKEY for more than 1 second according to manual requirements
    digitalWrite(MODEM_PWRKEY, HIGH);
    delay(100);
    digitalWrite(MODEM_PWRKEY, LOW);
    delay(1000);
    digitalWrite(MODEM_PWRKEY, HIGH);

    // Initialize the indicator as an output
    pinMode(LED_GPIO, OUTPUT);
    digitalWrite(LED_GPIO, LOW);
}

void initNetwork(){
  SerialMon.println("Initializing modem...");
  modem.init();
  // Turn off network status lights to reduce current consumption
  turnOffNetlight();
  
  // Unlock your SIM card with a PIN if needed
  if (strlen(simPIN) && modem.getSimStatus() != 3 ) {
    modem.simUnlock(simPIN);
  }

  Serial.print("Waiting for network...");

  if (!modem.waitForNetwork(240000L)) {
    SerialMon.println(" fail");
    delay(5000);
    return;
  }
  SerialMon.println(" OK");
  
  // When the network connection is successful, turn on the indicator
  digitalWrite(LED_GPIO, LED_ON);

  if (modem.isNetworkConnected()) {
    SerialMon.println("Network connected");
  }

  //connect to apn
  SerialMon.print("Connecting to APN: ");
  
  SerialMon.print(apn);
  if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
    SerialMon.println(" fail");
    
    delay(5000);
    return;
  }

  Serial.println(" OK");

  SerialMon.println("Signal: " + getSignal() + "/4");
}
// get request to specified endpoint
String getRequest(String endpoint){
  HttpClient http(client, server, port);

  // get the response code
  int httpResponseCode = http.get(endpoint);
  // set timeouts to 10s
  http.setHttpResponseTimeout(10000);
  http.setTimeout(10000);

  if (httpResponseCode != 0) {
    // failed to connect to the server abort
    requestSuccess = false;
    SerialMon.println("failed to connect to " + endpoint);
    SerialMon.println("response code: " + httpResponseCode);
    http.stop();
    return "Failed to connect";
  }
  // get the http status code
  int statusCode = http.responseStatusCode();
  SerialMon.println(statusCode);

  if (!statusCode) {
    // abort if there is no http status code
    requestSuccess = false;
    http.stop();
    return String(statusCode);
  }

  if(statusCode != 200){
    // if the status code is not 200 aka something failed in the get request then abort
    requestSuccess = false;
    http.stop();
    return "Not 200 status code";
  }
  
  int length = http.contentLength();
  if (length >= 0) {
    SerialMon.print(F("Content length is: "));
    SerialMon.println(length);
  }
  String body = http.responseBody();
  SerialMon.println(body);
  // Shutdown

  http.stop();
  requestSuccess = true;
  return body;
}
// post request to specified endpoint
String postRequest(String endpoint, String postData){
  HttpClient http(client, server, port);

  String contentType = "application/json";

  // get response code
  int httpResponseCode = http.post(endpoint, contentType, postData);

  // set timeouts to 15s
  http.setHttpResponseTimeout(15000);
  http.setTimeout(15000);

  if (httpResponseCode != 0) {
    // if we fail to connect to the server then abort
    requestSuccess = false;
    SerialMon.println("failed to connect to " + endpoint);
    http.stop();
    return "Failed to connect";
  }

  // get the http status code
  int statusCode = http.responseStatusCode();
  
  if (!statusCode) {
    http.stop();
    return String(statusCode);
  }

  SerialMon.print("HTTP Status code: ");
  SerialMon.println(statusCode);

  String body = http.responseBody();
  // Shutdown

  requestSuccess = true;
  http.stop();
  return body;
}

void getTemp(){
  // read temperature and humidity data from dht22
  hum = dht.readHumidity();
  temp = dht.readTemperature();
  SerialMon.println("Temp: " + String(temp) + " Hum: " + String(hum));
}

String getSignal(){
  // get modem signal and turn it into value on 1-4 scale
  // based on these marginals:
  // https://m2msupport.net/m2msupport/atcsq-signal-quality/
  
  int rssi = modem.getSignalQuality();
  signal = 0;
  if(rssi != 99){
    if(rssi < 10){
      // marginal
      signal = 1;
    }else if(rssi < 15){
      // ok
      signal = 2;
    }else if(rssi < 20){
      // good
      signal = 3;
    }else if(rssi < 32){
      // excellent
      signal = 4;
    }
  }else{
    // 99 signal means we couldnt determine the actual signal so we just return -1
    // this is usually due to poor signal quality
    signal = -1;
  }
  return(String(signal));
}

// check voltage from the voltage divider
int checkVoltage(){
  
  float sensorValue;
  
  // read value that comes from the module
  sensorValue = analogRead(voltageSensorPin);
  SerialMon.println(sensorValue);
  
  // calculate the actual voltage (differs around 0.2V from the actual value)
  voltageValue = (sensorValue / 4095) * 3.48 * 5.055; 
  SerialMon.println("V: " + String(voltageValue));
  // if the value is over 3 V then webasto is on else it is off
  if(voltageValue > 3){
    // pin has 3V
    hasVoltage = 1;
  }else{
    hasVoltage = 0;
  }
  return hasVoltage;
}
// send pulse to the relay that will then send pulse to the Telestart T91
void sendPulse(){
  SerialMon.println("Sending pulse");
  // turn the relayPin to high
  digitalWrite(relayPin, HIGH);
  delay(1000);
  // after 1s turn the relay pin to low again
  digitalWrite(relayPin, LOW);
  SerialMon.println("Pulse sent");
}

void setup()
{
    // Set console baud rate
    SerialMon.begin(115200);

    // set pinmodes accordingly
    pinMode(relayPin, OUTPUT);
    digitalWrite(relayPin, LOW);

    pinMode(tempSensorPin, INPUT);
    pinMode(voltageSensorPin, INPUT);
    Serial.println("Starting...");

    // wait for esp32 to start
    delay(500);
    
    // Start power management
    if (setupPMU() == false) {
        SerialMon.println("Setting power error");
    }
    // start the dht and get a reading
    dht.begin();
    getTemp();

    // setup the modem
    setupModem();
    // Set GSM module baud rate and UART pins
    SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
    initNetwork();
    
    // sync current time with ntp server
    // 12 = +3h and 8 = +2h
    modem.NTPServerSync("fi.pool.ntp.org", 12);
    
    // enable the deep sleep
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
}

void loop(){ 
    //if we lost network connection then try to connect to the APN again
    checkVoltage();
    if (!modem.isNetworkConnected()) {
      
      if(currentOnTime > savedMaxMillis && hasVoltage == 1){
        sendPulse();
      }else{
        currentOnTime = (millis() - startMillis);
      }
      
      SerialMon.println("Lost connection to network. Trying again.");
      
      modem.restart();
      initNetwork();
      return;
    }
    
    SerialMon.println("Signal: " + getSignal() + "/4");
    // get date and time from the gsm
    String time = modem.getGSMDateTime(DATE_TIME);
    String date = modem.getGSMDateTime(DATE_DATE);

    // take hours and minutes from the time string
    h = time.substring(0,2).toInt();
    m = time.substring(3,5).toInt();

    SerialMon.print("Current time: ");
    int currentTime = h * 100 + m;
    SerialMon.println(currentTime);

    // take day, month, year from date string
    day = date.substring(6,8).toInt();
    month = date.substring(3,5).toInt();
    year = date.substring(0,2).toInt();

    String currentDate = (String(day) + "/" + String(month) + "/" + String(year));
    
    SerialMon.print("Current date: ");
    SerialMon.println(currentDate);
  
    // create GET request to query current operation status
    SerialMon.println("Requesting phone data");
    String statusBody = getRequest(getStatus);
    
    SerialMon.println("Requesting car data");
    onOffSuccess = requestSuccess;
    String statusCarBody = getRequest(getStatusCar);
    
    SerialMon.println(statusBody);
    SerialMon.println(statusCarBody);
    // parse data that server sent
    int statusIndex = statusBody.indexOf(',');
    // search for the next comma just after the first one
    int onTimeIndex = statusBody.indexOf(',', statusIndex + 1);

    // get status, ontime and timestamp
    String _status = statusBody.substring(0, statusIndex);
    String _onTime = statusBody.substring(statusIndex + 1, onTimeIndex);
    pulseSent = statusCarBody.substring(69,70).toInt();
    String _timestamp = statusBody.substring(onTimeIndex + 1); // To the end of the string
    
    // cast status and onTime to ints
    status = _status.substring(10,11).toInt();
    onTime = _onTime.substring(9,11).toInt();

    // take year, month, day, hours and minutes from the timestamp that server sent us
    endYear = _timestamp.substring(15,17).toInt();
    endMonth = _timestamp.substring(18,20).toInt();
    endDay = _timestamp.substring(21,23).toInt();
    endH = _timestamp.substring(24,26).toInt();
    endM = _timestamp.substring(27,29).toInt();

    int timerStartTime = endH * 100 + endM;
    // add onTime amount to the timestamps minutes 
    endM = (endM + onTime);
    // if minutes exceeds 60 then add 1 to hours and substract 60 from minutes
    if(endM >= 60){
      endM = (endM - 60);
      endH = (endH + 1);
      // if day changes then change hours to 00
      if(endH >= 24){
        endH = 0;
      }
    }

    int timerEndTime = endH * 100 + endM;
   
    String timerDate = String(endDay) + "/" + String(endMonth) + "/" + String(endYear);
    
    SerialMon.print("Current date and time: ");
    SerialMon.println(currentDate + " | " + String(currentTime));

    SerialMon.print("Timer start up date and time: ");
    SerialMon.println(timerDate + " | " + String(timerStartTime));
    
    SerialMon.print("Timers end date and time: ");
    SerialMon.println(timerDate + " | " + String(timerEndTime));

    String timersBody = getRequest(getTimers);
    
    String timer1 = timersBody.substring(9,17);
    
    int h1 = timer1.substring(0,2).toInt();
    int m1 = timer1.substring(3,5).toInt();
    
    String timer2 = timersBody.substring(28,36);
    
    int h2 = timer2.substring(0,2).toInt();
    int m2 = timer2.substring(3,5).toInt();
    
    int timer1Enabled = timersBody.substring(48,49).toInt();
    int timer2Enabled = timersBody.substring(61,62).toInt();

    int timersOnTime = timersBody.substring(72,74).toInt();

    int timer1Formatted = h1 * 100 + m1;
    int timer2Formatted = h2 * 100 + m2;
    
    m1 = m1 + timersOnTime;
    // if minutes exceeds 60 then add 1 to hours and substract 60 from minutes
    if(m1 >= 60){
      m1 = (m1 - 60);
      h1 = (h1 + 1);
      // if day changes then change hours to 00
      if(h1 >= 24){
        h1 = 0;
      }
    }

    m2 = m2 + timersOnTime;
    if(m2 >= 60){
      m2 = (m2 - 60);
      h2 = (h2 + 1);
      // if day changes then change hours to 00
      if(h2 >= 24){
        h2 = 0;
      }
    }
    
    int timer1EndTime = h1 * 100 + m1;
    int timer2EndTime = h2 * 100 + m2;
    
    SerialMon.println(timer1Formatted);
    SerialMon.println(timer1EndTime);

    SerialMon.println(timer2Formatted);
    SerialMon.println(timer2EndTime);
    SerialMon.println(timer1Enabled);
    SerialMon.println(timer2Enabled);
    SerialMon.println(timersOnTime);

    // calculate max ontime in milliseconds
    if (timersOnTime > 0){
      maxMillis = timersOnTime * 60 * 1000;
    }

    // check if the webasto is on and if it is then calculate on time
    checkVoltage();
    if(hasVoltage == 1){
      currentOnTime = (millis() + deepSleepOffset) - startMillis;
      SerialMon.println("On time " + String(currentOnTime/1000) + " s");
    }else{
      currentOnTime = 0;
      deepSleepOffset = 0;
    }
    
    if(pulseSent == 0 && hasVoltage == 1 && status == 1 && currentOnTime/1000 > 1){
      pulseSent = 1;
    }

    // timer check if the webasto shold turn on based on the timers or to turn off or do nothing at all if the time isnt right

    // if webasto is ON and timer1/timer2 is enabled and we are over our endtime and webasto is on because of timer then turn it off
    // else if webasto is OFF and timer1/timer2 is enabled and we are over our startTime and webasto is off then turn the webasto on
    if((hasVoltage == 1 && (timer1Enabled == 1 && currentTime >= timer1EndTime) || (timer2Enabled == 1 && currentTime >= timer2EndTime)) && timerOn){
      sendPulse();
      startMillis = millis();
      currentOnTime = 0;
      status = 0;
      timerOn = false;
      pulseSent = 1;
      SerialMon.println("(timer) Webasto should turn off");
      // status 0
      // create POST request to the server to update the current status according to "status" variable
      String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + -1 + "\",\"rssi\":\"" + 20 + "\"}";
      SerialMon.println(postStatusData);
      SerialMon.println(postRequest(updateStatusPhone, postStatusData));

      int _timerStartTime;
      if(currentTime >= timer1EndTime){
        _timerStartTime = timer1EndTime;
      }else{
        _timerStartTime = timer2EndTime;
      }

      String postLogsData = "{\"api_key\":\"" + api_key + "\",\"startTime\":\"" + _timerStartTime*100 + "\",\"endTime\":\"" + currentTime*100 + "\",\"onTime\":\"" + currentOnTime/1000/60 + "\"}";
      SerialMon.println(postLogsData);
      SerialMon.println(postRequest(postLogs, postLogsData));
        
    }else if((hasVoltage == 0 && timer1Enabled == 1 && currentTime >= timer1Formatted && currentTime <= timer1EndTime && !timerOn) || (hasVoltage == 0 && timer2Enabled == 1 && currentTime >= timer2Formatted && currentTime <= timer2EndTime && !timerOn)){ // webasto is off
      sendPulse();
      startMillis = millis();
      savedMaxMillis = maxMillis;
      currentOnTime = 0;
      pulseSent = 0;
      SerialMon.println("(Timer) Webasto should be on for " + String(maxMillis/1000/60) + " m");
      timerOn = true;

      // status 0
      // create POST request to the server to update the current status according to "status" variable
      String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 1 + "\",\"onTime\":\"" + timersOnTime + "\",\"pulseSent\":\"" + 1 + "\",\"rssi\":\"" + 40 + "\"}";
      SerialMon.println(postStatusData);
      SerialMon.println(postRequest(updateStatusPhone, postStatusData));
    }

    // if webasto is on then calculate maxMillisOnOff
    if (onTime > 0){
      maxMillisOnOff = onTime * 60 * 1000;
    }
    
    // check for 12VDC on pin 3 before we try to change turn the webasto on or off via the button
    
    SerialMon.println("hasVoltage " + String(hasVoltage));
    SerialMon.println("status " + String(status));
    SerialMon.println("pulsesent " + String(pulseSent));
    SerialMon.println("timerOn " + String(timerOn));
    
    // check if webasto is on
    if(hasVoltage == 1){
      SerialMon.println("Webasto is on");
    }else{
      timerOn = false;
      SerialMon.println("Webasto is off");
    }

    // turn on the webasto via the start button on the app
    // if we did successfull request to the server and webasto isnt on because of timer,
    // but status is 1 and we havent sent pulse yet and we are within the start and end time then turn webasto on

    // turn off the webasto via the stop button on the app
    // else if we did successfull request to the server and webasto isnt on because of timer but status is 0 
    // and we have sent the pulse and we are over the time that is stored in the db then shut the webasto down

    // webasto should be turned off because it has been on for specified amount and it was turned on by the button on the app

    if(onOffSuccess && !timerOn && status == 1 && pulseSent == 0 && currentTime >= timerStartTime && currentTime <= timerEndTime){ // webasto should be turned on because of on cmd
      if(hasVoltage == 0){ // check if webasto is off
        sendPulse();
        pulseSent = 1;
        
        startMillis = millis();
        savedMaxMillis = maxMillis;
        String postStatusDataCar = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + status + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 100 + "\"}";
        SerialMon.println(postStatusDataCar);
        SerialMon.println(postRequest(updateStatus, postStatusDataCar));
        
        String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + status + "\",\"onTime\":\"" + onTime + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 100 + "\"}";
        SerialMon.println(postStatusData);
        SerialMon.println(postRequest(updateStatusPhone, postStatusData));
  
        SerialMon.println("(on off) Webasto should turn ON");
      }
     
    }else if(onOffSuccess && !timerOn && status == 0 && pulseSent == 1 && currentTime >= timerStartTime){
      if(hasVoltage == 1){ // check if webasto is on
        sendPulse();
       
        delay(500);
        checkVoltage();
        
        if(hasVoltage == 1){
          pulseSent = 1;
          
          SerialMon.println("(on off) Tried turning webasto OFF but it didnt work");
        }else{
          pulseSent = 0;
          startMillis = millis();
          SerialMon.println("(on off) Webasto should turn OFF");
        }

        String postStatusDataCar = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 150 + "\"}";
        SerialMon.println(postStatusDataCar);
        SerialMon.println(postRequest(updateStatus, postStatusDataCar));
        
        String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 150 + "\"}";
        SerialMon.println(postStatusData);
        SerialMon.println(postRequest(updateStatusPhone, postStatusData));
        
        String postLogsData = "{\"api_key\":\"" + api_key + "\",\"startTime\":\"" + timerStartTime*100 + "\",\"endTime\":\"" + currentTime*100 + "\",\"onTime\":\"" + currentOnTime/1000/60 + "\"}";
        SerialMon.println(postLogsData);
        SerialMon.println(postRequest(postLogs, postLogsData));
      }else{
        
        pulseSent = 0;
        String postStatusDataCar = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 155 + "\"}";
        SerialMon.println(postStatusDataCar);
        SerialMon.println(postRequest(updateStatus, postStatusDataCar));
        
        String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 155 + "\"}";
        SerialMon.println(postStatusData);
        SerialMon.println(postRequest(updateStatusPhone, postStatusData));
        
        String postLogsData = "{\"api_key\":\"" + api_key + "\",\"startTime\":\"" + timerStartTime*100 + "\",\"endTime\":\"" + currentTime*100 + "\",\"onTime\":\"" + currentOnTime/1000/60 + "\"}";
        SerialMon.println(postLogsData);
        SerialMon.println(postRequest(postLogs, postLogsData));
        
        SerialMon.println("(on off) Webasto should be turned OFF but it is already OFF");
      }
      
    }else if(!timerOn && status == 1 && pulseSent == 1 && currentTime >= timerEndTime){ // (currentOnTime > maxMillisOnOff && pulseSent == 1) || 
      if(hasVoltage == 1){

        sendPulse();
        delay(500);
        checkVoltage();
        
        if(hasVoltage == 1){
          pulseSent = 1;
          
          SerialMon.println("(on off) Tried turning webasto OFF because it has been on for specified time but it didnt work");
        }else{
          pulseSent = 0;
          startMillis = millis();
          SerialMon.println("(on off) Webasto should turn OFF because it has been on for specified amount");
        }
 
        String postStatusDataCar = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 200 + "\"}";
        SerialMon.println(postStatusDataCar);
        SerialMon.println(postRequest(updateStatus, postStatusDataCar));
     
        String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + 0 + "\",\"onTime\":\"" + 0 + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + 200 + "\"}";
        SerialMon.println(postStatusData);
        SerialMon.println(postRequest(updateStatusPhone, postStatusData));
        
        String postLogsData = "{\"api_key\":\"" + api_key + "\",\"startTime\":\"" + timerStartTime*100 + "\",\"endTime\":\"" + currentTime*100 + "\",\"onTime\":\"" + currentOnTime/1000/60 + "\"}";
        SerialMon.println(postLogsData);
        SerialMon.println(postRequest(postLogs, postLogsData));
      }
      
    }

    // check for 12VDC on pin 3 before after we tried changing it and send it to the server
    SerialMon.println("Checking voltage");
    checkVoltage();
    SerialMon.println("Posting voltage data");
    String postVoltData = "{\"api_key\":\"" + api_key + "\",\"voltage\":\"" + voltageValue + "\"}";
    postRequest(updateVoltage, postVoltData);
    SerialMon.println("Signal: " + getSignal() + "/4");
    
    // create POST request to the server to update the current status according to "status" variable
    SerialMon.println("Posting current status data");
    int _currentOnTime = currentOnTime/1000/60;
    String postStatusData = "{\"api_key\":\"" + api_key + "\",\"newStatus\":\"" + hasVoltage + "\",\"onTime\":\"" + _currentOnTime + "\",\"pulseSent\":\"" + pulseSent + "\",\"rssi\":\"" + signal + "\"}";
    SerialMon.println(postStatusData);
    SerialMon.println(postRequest(updateStatus, postStatusData));

    // read temperature and humidity data
    getTemp();

    // check if temp and hum readings are actual values and not nan
    if(!isnan(temp) || !isnan(hum)){
      SerialMon.println("Posting temperature and humidity data");
      // make a POST request to update the current temperature
      String postDataTemp = "{\"api_key\":\"" + api_key + "\",\"temp\":\"" + temp + "\",\"hum\":\"" + hum + "\"}";
      SerialMon.println(postRequest(postTemp, postDataTemp));
    }
    
    SerialMon.print("Webasto status: ");
    SerialMon.println(hasVoltage);
    SerialMon.println("Ontime " + String(onTime) + " min");

    // check if webasto is on
    if(hasVoltage == 1){
      deepSleepOffset = deepSleepOffset + millis() + (TIME_TO_SLEEP * 1000);
    }

    SerialMon.println("Sleeping");
    SerialMon.flush(); 
    esp_deep_sleep_start();
}
